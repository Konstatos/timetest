﻿using System;
using System.IO;
using TestLibrary;

namespace Core
{
    class Program
    {
        static Tests t = new Tests();
        static void Main(string[] args)
        {
            var res = t.TestAndSave(".NET CORE");
            File.WriteAllText($"result_{DateTime.Now.ToFileTime()}.txt", res);
        }
    }
}
