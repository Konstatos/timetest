﻿using Newtonsoft.Json;
using System.Web.Mvc;
using TestLibrary;

namespace TimeTestAspNet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //ViewBag.Result = BaseTimeTest.I.GetAllDataString();
            ViewBag.Result = BaseTimeTest.I.GetAllData();
            return View();
        }

        public string GetData()
        {
            return JsonConvert.SerializeObject(BaseTimeTest.I.GetAllData());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}