﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestLibrary;
using TimeTestAsp.Models;

namespace TimeTestAsp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly Tests _tests = new Tests();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult TimeTest()
        {
            ViewBag.Result = _tests.TestAndSave("ASP.NET Core 5");
            return View();
        }
        public IActionResult TimeAllTests()
        {
            ViewBag.Result = BaseTimeTest.I.GetAllDataString();
            return View();
        }

        public IActionResult Index()
        {
            //ViewBag.Result = _tests.TestAndSave("ASP.NET Core 5");
            ViewBag.Result = BaseTimeTest.I.GetAllDataString();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
