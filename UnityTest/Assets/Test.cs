using TestLibrary;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    private string Platform = "Unity 2020.3.10, .NET Standard, ill2cpp, API-24-29";
    public Text Text;
    readonly Tests _tests = new Tests();

    private void Start()
    {
        Platform = Application.unityVersion + ", Android-24-29";
#if NET_STANDARD_2_0
        Platform += ", .NET Standard";
#else
        Platform+= ", .NET4x";
#endif

#if ENABLE_MONO
        Platform += ", mono";
#else
        Platform+= ", ill2cpp";
#endif

        Text.text = "�������, ����� ��������� ����: " + Platform;
    }

    public void DoTest()
    {
        Text.text = "���� �����...";
        var s = _tests.TestAndSave(Platform);
        Text.text = "���� ��������";
        Debug.Log(s);
    }
}
