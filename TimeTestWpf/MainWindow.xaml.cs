﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using TestLibrary;

namespace TimeTestWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly Tests _tests = new Tests();

        public MainWindow()
        {
            InitializeComponent();
            AddMenu("Time test", () => _tRes.Text = _tests.TestAndSave(".NET Framework WPF"));
            AddMenu("Результаты полностью", () => _tRes.Text = BaseTimeTest.I.GetAllDataString());
            AddMenu("Результаты", () => _tRes.Text = BaseTimeTest.I.GetGroupDataString());
        }

        Button AddMenu(string text, Action action) =>
            _pRoot.AddMenu(text, action);

        //private void AddMenu(string title, Func<object> getWindow)
        //{
        //    var m = new MenuItem();
        //    m.Header = title;
        //    m.Click += (s, e) => _cRoot.Content = getWindow();
        //    _mMenu.Items.Add(m);
        //}
    }

    public static class UiHelper
    {
        public static Button AddMenu(this StackPanel menu, string text, Action action)
        {
            var i = new Button() { Content = text };
            menu.Children.Add(i);
            i.Click += (s, e) =>
            {
                //LogInit("");
                //_tRes.Text = "Запущена задача. Ждём...";
                var sw = Stopwatch.StartNew();
                //try
                //{
                action();
                //}
                //catch (Exception ex)
                //{
                //    CLog.Error("AddMenu: " + ex.ToString());
                //}
                //Log($"\nЗавершено за {sw.ElapsedMilliseconds / 1000f} c");
            };
            return i;
        }
    }
}
