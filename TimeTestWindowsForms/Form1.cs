﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestLibrary;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        readonly Tests _tests = new Tests();
        Task Task;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Task = Task.Run(() =>
            {
                var t = _tests.TestAndSave(".NET Framework Windows Forms");
                Invoke((Action)(() => textBox1.Text = t));
            });
            textBox1.Text = "Запущены тесты. Ждём...";
        }
    }
}
