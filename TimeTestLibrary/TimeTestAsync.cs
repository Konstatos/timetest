﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace TestLibrary
{
    /// <summary>
    /// Механизм асинхронного замера быстродействия
    /// (c) Konstatos
    /// </summary>
    public class TimeTestAsync
    {
        public int TimeMilliseconds = 1;

        private Action TestAction = () => { };
        private Thread Thread;
        public int Count = 0;
        public int Elements = 0;

        public readonly Dictionary<string, List<TimeResult>> Results = 
            new Dictionary<string, List<TimeResult>>();

        private void Init()
        {
            if (Thread != null) return;
            Thread = new Thread(TestFunk); // { IsBackground = true };
            Thread.Start();
        }

        ~TimeTestAsync() => Stop();

        public void Zamer(string info, Action action, int elements = 0)
        {
            Init();
            if (!Results.ContainsKey(info))
                Results.Add(info, new List<TimeResult>());
            var z = Zamer(action);
            Results[info].Add(z);
            Elements = elements;
        }

        private TimeResult Zamer(Action action)
        {
            var gc = GC.CollectionCount(0);
            TestAction = action;
            Count = 0;
            var sw = Stopwatch.StartNew();
            Thread.Sleep(TimeMilliseconds);
            sw.Stop();
            return new TimeResult()
            {
                Count = Count,
                Time = sw.Elapsed,
                Elements = Elements,
                GC = GC.CollectionCount(0) - gc
            };
        }

        private void TestFunk()
        {
            while (true)
            {
                TestAction.Invoke();
                ++Count;
            }
        }

        public void Stop()
        {
            try
            {
                Thread?.Abort();
                Thread = null;
            }
            catch (Exception e)
            {
                // сообщение об ошибке
            }
        }
    }

    public class PcInfo
    {
        public string Name;
        public int ProcCount;
        public string Version;
        public string OSVersion;
        public int SystemPageSize;
        public bool Is64BitOS;

        public string ToStringSum() => $"{Name} {ProcCount}CPU {OSVersion}";
        public string ToStringTitle() => $"Name\tProcCount\tVersion\tOSVersion\tSystemPageSize\tIs64BitOS";
        public string ToStringData() => $"{Name}\t{ProcCount}\t{Version}\t{OSVersion}\t{SystemPageSize}\t{Is64BitOS}";
    }

    public class TimeTestResult : IEntity
    {
        private const string Br = "\t";

        public List<KeyValuePair<string, List<TimeResult>>> Results;
        public PcInfo PcInfo;
        public string Platform;
        public bool IsDebug;
        public bool IsDebuggerAttached;
        public bool IsGC;
        public DateTime Date;

        public TimeTestResult() { }

        public TimeTestResult(Dictionary<string, List<TimeResult>> results)
        {
            Results = results.ToList();
            PcInfo = new PcInfo()
            {
                Name = Environment.MachineName,
                ProcCount = Environment.ProcessorCount,
                Version = Environment.Version.ToString(),
                OSVersion = Environment.OSVersion.VersionString,
                SystemPageSize = Environment.SystemPageSize,
                Is64BitOS = Environment.Is64BitOperatingSystem
            };
            Date = DateTime.UtcNow;
#if DEBUG
            IsDebug = true;
#else
            IsDebug = false;
#endif
            NewId();
        }

        public string ToStringTitle() =>
            $"Date\tPlatform\tIsDebug\tIsDebuggerAttached\t{PcInfo.ToStringTitle()}";
        public string ToStringData() =>
            $"{Date.ToShortDateString()}\t{Platform}\t{IsDebug}\t{IsDebuggerAttached}\t{PcInfo.ToStringData()}";
        public List<object> ToListData() =>
            new List<object>() { Platform, IsDebug, IsDebuggerAttached }; //, PcInfo.Name\tProcCount\tVersion\tOSVersion\tSystemPageSize\tIs64BitOS };

        public string ToStringAll()
        {
            var s = ToStringTitleAll();
            var d = ToStringDataAll();
            return s + "\n" + d;
        }

        public string ToStringDataAll(string prefix = null)
        {
            var sb = new StringBuilder();
            foreach (var res in Results)
            {
                var byCount = res.Value.Where(x => x.Count > 0).GroupBy(x => x.Elements);
                foreach (var r in byCount)
                {
                    var withResults = r.ToArray();
                    var nano = withResults.Select(x => x.Nanoseconds()).ToList();
                    if (nano.Count == 0) continue;
                    nano = nano.OrderBy(x => x).ToList();
                    var avg = nano.Average(x => x);
                    var med = nano[nano.Count / 2];
                    sb.AppendLine(
                         $"{prefix} {res.Key}{Br}" +
                         $"{r.Key}{Br}" +
                         $"{avg:F2}{Br}" +
                         $"{med:F2}{Br}" +
                         $"{StandardDeviation(nano):F2}{Br}" +
                         $"{withResults.Average(x => (double)x.Count):F0}{Br}" +
                         $"{withResults.Average(x => x.Time.TotalMilliseconds):F2}{Br}" +
                         $"{withResults.Length}{Br}" +
                         $"{withResults.Average(x => (double)x.GC):F2}");
                }
            }
            return sb.ToString();
        }

        public static double StandardDeviation(List<double> valueList)
        {
            double M = 0.0;
            double S = 0.0;
            int k = 1;
            foreach (double value in valueList)
            {
                double tmpM = M;
                M += (value - tmpM) / k;
                S += (value - tmpM) * (value - M);
                k++;
            }
            return Math.Sqrt(S / (k - 2));
        }

        public static string ToStringTitleAll() =>
            $"Функция{Br}" +
            $"Элементы{Br}" +
            $"Среднее время на выполнение, нс{Br}" +
            $"Медиананное время на выполнение, нс{Br}" +
            $"Среднеквадратичное отклонение{Br}" +
            $"Среднее к-во запусков за тест, раз{Br}" +
            $"Среднее Время теста, мс{Br}" +
            $"К-во тестов, раз{Br}" +
            $"Среднее к-во вызовов сборки мусора на тест, раз";
    }
}