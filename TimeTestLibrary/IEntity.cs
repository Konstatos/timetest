﻿using System;

namespace TestLibrary
{
    public class IEntity
    {
        public string _id;
        public string OwnerId;
        public string Name;
        public int Version;

        public void NewId()
        {
            _id = Guid.NewGuid().ToString("N");
        }

        public override string ToString()
        {
            return $"{GetType().Name} {Name} ({_id})";
        }
    }
}
