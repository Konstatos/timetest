﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TestLibrary
{
    /// <summary>
    /// Тесты быстродействия отдельных операций
    /// (c) Konstatos
    /// </summary>
    public class Tests
    {
        readonly TimeTestAsync Tester = new TimeTestAsync();

        private float ClassProperty { get; set; }
        static float StaticProperty { get; set; }

        static float StaticField = 0;

        float ClassFieldFloat = 0;
        byte ClassFieldByte = 0;
        int ClassFieldInt = 0;
        decimal ClassFieldDecimal = 0;
        double ClassFieldDouble = 0;

        float ClassFieldFloat2 = 0;
        string ClassStr = "";
        bool ClassBool = false;
        StringBuilder ClassStringBuilder = new StringBuilder();
        public bool _isGC = true;

        void ClassFunc() { }
        static void StaticFunc() { }
        void ClassFuncPlus() { ++ClassFieldFloat; }
        static void StaticFuncPlus() { ++StaticField; }

        private const int Min = 50, Max = 100;

        void TryGC()
        {
            if (_isGC) GC.Collect();
        }

        public void Test()
        {
            var localRandom = new Random();
            //GC.TryStartNoGCRegion(4 * 1024 * 1024);
            //GCSettings.LatencyMode = GCLatencyMode.LowLatency;

            for (int i = 0; i <= 10; i++)
            {
                ClassStr = "";
                ClassStringBuilder = new StringBuilder();
                ClassFieldFloat = localRandom.Next(Min, Max);
                ClassFieldFloat2 = localRandom.Next(Min, Max);
                StaticField = localRandom.Next(Min, Max);
                ClassProperty = localRandom.Next(Min, Max);
                StaticProperty = localRandom.Next(Min, Max);
                float localField = localRandom.Next(Min, Max);

                var List = new List<float>();
                var ListA = new List<float>();
                var Stack = new Stack<float>();
                var StackA = new Stack<float>();
                var HashSet = new HashSet<float>();
                var HashSetA = new HashSet<float>();
                var Dictionary = new Dictionary<float, float>();
                var DictionaryA = new Dictionary<float, float>();
                var SortedList = new SortedList<float, float>();
                var SortedListA = new SortedList<float, float>();
                var BlockingCollection = new BlockingCollection<float>();
                var BlockingCollectionA = new BlockingCollection<float>();
                var ObservableCollection = new ObservableCollection<float>();
                var ObservableCollectionA = new ObservableCollection<float>();

                var Str1 = "1";
                var Str10 = "1234567890";
                var Str100 = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
                var StrArr10 = new string[] { "123", "123", "123", "123", "123", "123", "123", "123", "123", "123", };

                TryGC();
                //GC.WaitForFullGCComplete(-1);

                // Func
                Tester.Zamer("ClassFunc() { }", ClassFunc);
                Tester.Zamer("StaticFunc() { }", StaticFunc);
                Tester.Zamer("ClassFunc() { ++ClassField; }", ClassFuncPlus);
                Tester.Zamer("StaticFunc() { ++StaticField; }", StaticFuncPlus);
                Tester.Zamer("ClassFunc()", () => { ClassFunc(); });
                Tester.Zamer("StaticFunc()", () => { StaticFunc(); });
                Tester.Zamer("() => { }", () => { });

                // ++
                Tester.Zamer("ClassField++", () => { ClassFieldFloat++; });
                Tester.Zamer("++ClassField", () => { ++ClassFieldFloat; });
                Tester.Zamer("++localField", () => { ++localField; });
                Tester.Zamer("++StaticField", () => { ++StaticField; });
                Tester.Zamer("++ClassProperty", () => { ++ClassProperty; });
                Tester.Zamer("++StaticProperty", () => { ++StaticProperty; });
                Tester.Zamer("if (true) ++ClassField", () => { if (true) ++ClassFieldFloat; });
                Tester.Zamer("if (false) ++ClassField", () => { if (false) ++ClassFieldFloat; });
                Tester.Zamer("if (++ClassField > 1) ClassField = 0", () => { if (++ClassFieldFloat == 2) ClassFieldFloat = 0; });
                Tester.Zamer("x = ClassField + 1.1f", () => ClassFieldFloat2 = ClassFieldFloat + 1.1f);
                Tester.Zamer("x = ClassField - 1.1f", () => ClassFieldFloat2 = ClassFieldFloat - 1.1f);
                Tester.Zamer("x = ClassField * 1.1f", () => ClassFieldFloat2 = ClassFieldFloat * 1.1f);
                Tester.Zamer("x = ClassField / 1.1f", () => ClassFieldFloat2 = ClassFieldFloat / 1.1f);
                Tester.Zamer("x = ClassField % 1.1f", () => ClassFieldFloat2 = ClassFieldFloat % 1.1f);

                Tester.Zamer("ClassField += 1.1f", () => ClassFieldFloat += 1.1f);
                Tester.Zamer("ClassFieldByte += 2", () => ClassFieldByte += 2);
                Tester.Zamer("ClassFieldInt += 2", () => ClassFieldInt += 2);
                //Tester.Zamer("ClassField += 1.1f", () =>
                //{
                //    for (int j = 0; j < 1000; j++)
                //        ClassFieldFloat += 1.1f;
                //});
                //Tester.Zamer("ClassFieldByte += 2", () =>
                //{
                //    for (int j = 0; j < 1000; j++)
                //        ClassFieldByte += 2;
                //});
                //Tester.Zamer("ClassFieldInt += 2", () =>
                //{
                //    for (int j = 0; j < 1000; j++)
                //        ClassFieldInt += 2;
                //});
                Debug.Assert(!float.IsNaN(ClassFieldFloat));
                ClassFieldFloat = localRandom.Next(Min, Max);
                TryGC();

                #region Math
                Tester.Zamer("Math.Sin(++i)", () => Math.Sin(--ClassFieldFloat));
                Tester.Zamer("Math.Cos(++i)", () => Math.Cos(++ClassFieldFloat));
                Tester.Zamer("Math.Tan(++i)", () => Math.Tan(--ClassFieldFloat));
                Tester.Zamer("Math.Atan(++i)", () => Math.Atan(++ClassFieldFloat));
                Tester.Zamer("Math.Atan2(++i, i)", () => Math.Atan2(--ClassFieldFloat, ClassFieldFloat));

                Tester.Zamer("Math.Abs(++i)", () => Math.Abs(++ClassFieldFloat));
                ClassFieldFloat = 1;
                Tester.Zamer("Math.Log(++i)", () => Math.Log(++ClassFieldFloat));
                ClassFieldFloat = 1;
                Tester.Zamer("Math.Pow(++i, 2)", () => Math.Pow(++ClassFieldFloat, 2));
                ClassFieldFloat = 1;
                Tester.Zamer("Math.Pow(++i, 1.4)", () => Math.Pow(++ClassFieldFloat, 1.4));
                ClassFieldFloat = 1.3f;
                Tester.Zamer("Math.Sqrt(++i)", () => Math.Sqrt(++ClassFieldFloat));
                Tester.Zamer("Math.Max(i, ++i)", () => Math.Max(ClassFieldFloat, ++ClassFieldFloat));
                Tester.Zamer("Math.Min(i, ++i)", () => Math.Min(ClassFieldFloat, --ClassFieldFloat));
                Tester.Zamer("Math.Min(++i, 100)", () => Math.Min(++ClassFieldFloat, 100));

                Debug.Assert(!float.IsNaN(ClassFieldFloat));
                ClassFieldFloat = localRandom.Next(Min, Max);
                TryGC();

                // Round
                Tester.Zamer("Math.Round(++i)", () => Math.Round(--ClassFieldFloat));
                Tester.Zamer("Math.Round(++i, 2)", () => Math.Round(++ClassFieldFloat, 2));
                Tester.Zamer("Math.Round(65.5633, 2)", () => Math.Round(65.5633, 2));
                Tester.Zamer("Math.Round(0d, 2)", () => Math.Round(0d, 2));
                Tester.Zamer("Math.Round(0m, 2)", () => Math.Round(0m, 2));
                Tester.Zamer("Math.Ceiling(++i)", () => Math.Ceiling(--ClassFieldFloat));
                Tester.Zamer("Math.Floor(++i)", () => Math.Floor(++ClassFieldFloat));
                Tester.Zamer("Math.Truncate(++i)", () => Math.Truncate(--ClassFieldFloat));

                #endregion

                #region Random
                Tester.Zamer("ClassField = localRandom.Next(10)", () => ClassFieldFloat = localRandom.Next(10));
                Tester.Zamer("localRandom.Next(10)", () => localRandom.Next(10));
                Tester.Zamer("localRandom.Next(10, 10000)", () => localRandom.Next(10, 10000));
                Tester.Zamer("localRandom.NextDouble()", () => localRandom.NextDouble());
                Tester.Zamer("new Random().Next(10)", () => new Random().Next(10));
                TryGC();
                #endregion

                // ToString
                Tester.Zamer("0.ToString()", () => 0.ToString());
                Tester.Zamer("10.ToString()", () => 10.ToString());
                Tester.Zamer("100000.ToString()", () => 100000.ToString());
                Tester.Zamer("10f.ToString()", () => 10f.ToString());
                Tester.Zamer("10d.ToString()", () => 10d.ToString());
                Tester.Zamer("10m.ToString()", () => 10m.ToString());
                Tester.Zamer("10.1234567890f.ToString()", () => 10.1234567890f.ToString());
                Tester.Zamer("10.1234567890d.ToString()", () => 10.1234567890d.ToString());
                Tester.Zamer("10.1234567890.ToString(\"F2\")", () => 10.1234567890.ToString("F2"));
                Tester.Zamer("10.1234567890.ToString(\"##.###\")", () => 10.1234567890.ToString("##.###"));
                TryGC();

                // String
                Tester.Zamer("ClassStr = \"S1\"", () => ClassStr = "S1");
                Tester.Zamer("ClassStr = \"S1\" + \"S2\"", () => ClassStr = "S1" + "S2");
                Tester.Zamer("ClassStr = \"S1\" + ++ClassField", () => ClassStr = "S1" + ++ClassFieldFloat);
                Tester.Zamer("ClassStr += \"S1\"", () => ClassStr += "S1");
                Tester.Zamer("ClassStringBuilder.Append(\"S1\")", () => ClassStringBuilder.Append("S1"));
                TryGC();

                Tester.Zamer("ClassStr = string.Format(\"S1{0}S2\", ++ClassField)", () => ClassStr = string.Format("S1{0}S2", ++ClassFieldFloat));
                Tester.Zamer("ClassStr = $\"S1{++ClassField}S2\"", () => ClassStr = $"S1{++ClassFieldFloat}S2");
                Tester.Zamer("ClassStr = \"S1\" + ++ClassField + \"S2\"", () => ClassStr = "S1" + ++ClassFieldFloat + "S2");
                Tester.Zamer("ClassStr = \"S1\" + ++Class ... Field + \"S6\"", () => ClassStr = "S1" + ++ClassFieldFloat + "S2" + ++ClassFieldFloat + "S3" + ++ClassFieldFloat + "S4" + ++ClassFieldFloat + "S5" + ++ClassFieldFloat + "S6");
                Tester.Zamer("ClassStr = $\"S1{++ClassField}S2{++Class ... Field}S6\"", () => ClassStr = $"S1{++ClassFieldFloat}S2{++ClassFieldFloat}S3{++ClassFieldFloat}S4{++ClassFieldFloat}S5{++ClassFieldFloat}S6");
                Tester.Zamer("ClassStr = string.Join(\", \", StrArr10)", () => ClassStr = string.Join(",", StrArr10));

                Tester.Zamer("ClassStr = DateTime.Now.ToString()", () => ClassStr = DateTime.Now.ToString());
                Tester.Zamer("ClassStr = DateTime.Now.ToString(\"F\")", () => ClassStr = DateTime.Now.ToString("F"));
                Tester.Zamer("ClassStr = DateTime.Now.ToString(\"yyyy.MM.dd\")", () => ClassStr = DateTime.Now.ToString("yyyy.MM.dd"));
                TryGC();

                Tester.Zamer("ClassBool = Str1.Contains(\"123\")", () => ClassBool = Str1.Contains("123"));
                Tester.Zamer("ClassBool = Str10.Contains(\"123\")", () => ClassBool = Str10.Contains("123"));
                Tester.Zamer("ClassBool = Str10.Contains(\"321\")", () => ClassBool = Str10.Contains("321"));
                Tester.Zamer("ClassBool = Str10.Contains('1')", () => ClassBool = Str10.Contains('1'));
                Tester.Zamer("ClassBool = Str10.Contains('-')", () => ClassBool = Str10.Contains('-'));
                Tester.Zamer("ClassBool = Str100.Contains(\"123\")", () => ClassBool = Str100.Contains("123"));
                Tester.Zamer("ClassBool = Str100.Contains('1')", () => ClassBool = Str100.Contains('1'));
                Tester.Zamer("ClassBool = Str100.Contains('-')", () => ClassBool = Str100.Contains('-'));
                Tester.Zamer("ClassBool = Str1 == \"123\"", () => ClassBool = Str10 == "123");
                Tester.Zamer("ClassBool = Str10.Equals(\"123\")", () => ClassBool = Str10.Equals("123"));
                Tester.Zamer("ClassBool = string.IsNullOrEmpty(Str10)", () => ClassBool = string.IsNullOrEmpty(Str10));
                Tester.Zamer("ClassBool = string.IsNullOrWhiteSpace(Str10)", () => ClassBool = string.IsNullOrWhiteSpace(Str10));
                TryGC();

                Tester.Zamer("ClassStr = Str10.Trim()", () => ClassStr = Str10.Trim());
                Tester.Zamer("ClassStr = Str100.Trim()", () => ClassStr = Str100.Trim());
                Tester.Zamer("ClassStr = Str10.Substring(5)", () => ClassStr = Str10.Substring(5));
                Tester.Zamer("ClassStr = Str100.Substring(5)", () => ClassStr = Str100.Substring(5));
                TryGC();

                // Collection Add
                Tester.Zamer("List.Add(++ClassField)", () => ListA.Add(++ClassFieldFloat));
                Tester.Zamer("Stack.Push(++ClassField)", () => StackA.Push(++ClassFieldFloat));
                Tester.Zamer("HashSet.Add(++ClassField)", () => HashSetA.Add(++ClassFieldFloat));
                TryGC();
                Tester.Zamer("Dictionary.Add(++ClassField, ClassField)", () => DictionaryA.Add(++ClassFieldFloat, ClassFieldFloat));
                Tester.Zamer("SortedList.Add(++ClassField, ClassField)", () => SortedListA.Add(++ClassFieldFloat, ClassFieldFloat));
                TryGC();
                Tester.Zamer("BlockingCollection.Add(++ClassField)", () => BlockingCollectionA.Add(++ClassFieldFloat));
                Tester.Zamer("ObservableCollection.Add(++ClassField)", () => ObservableCollectionA.Add(++ClassFieldFloat));

                //Tester.Zamer("GC.Collect()", () => GC.Collect());

                #region Рефлексия

                object o;
                Tester.Zamer($"1.GetType()", () => 1.GetType());
                Tester.Zamer($"o = 1.GetType().Name", () => o = 1.GetType().Name);
                Tester.Zamer($"o = List.GetType().Name", () => o = List.GetType().Name);
                Tester.Zamer($"o = 1.GetType().FullName", () => o = 1.GetType().FullName);
                Tester.Zamer($"o = List.GetType().GetProperty(\"Count\")", () => o = List.GetType().GetProperty("Count"));
                Tester.Zamer($"o = List.GetType().GetProperty(\"Count\").GetValue(List)", () => o = List.GetType().GetProperty("Count").GetValue(List));
                Tester.Zamer($"1.GetType() (2й раз)", () => 1.GetType());

                int di = 0;
                dynamic dynamicInt = 1;
                dynamic dynamicStr = "S1";
                Tester.Zamer($"dynamicInt++", () => dynamicInt++);
                Tester.Zamer($"dynamicInt = ++i", () => dynamicInt = ++di);
                Tester.Zamer($"int += dynamicInt", () => di += dynamicInt);
                Tester.Zamer($"ClassStringBuilder.Append(dynamicStr)", () => ClassStringBuilder.Append(dynamicStr));

                #endregion


                for (int n = 1; n < 10000; n *= 10)
                {
                    List.Clear();
                    Stack.Clear();
                    HashSet.Clear();
                    Dictionary.Clear();
                    SortedList.Clear();
                    ObservableCollection.Clear();
                    BlockingCollection = new BlockingCollection<float>();

                    var Array = new float[n];
                    for (int j = 0; j < n; j++)
                    {
                        var rnd = localRandom.Next(n * 2);
                        Array[j] = rnd;
                        List.Add(rnd);
                        Stack.Push(rnd);
                        HashSet.Add(j);
                        Dictionary.Add(j, rnd);
                        SortedList.Add(j, rnd);
                        ObservableCollection.Add(rnd);
                        BlockingCollection.Add(rnd);
                    }

                    #region Функции коллекций
                    var ExistElement = List[localRandom.Next(List.Count)];
                    var NotExistElement = List.Max() + 5;
                    Tester.Zamer($"List.Find(x => x == ExistElement)", () => List.Find(x => x == ExistElement), n);
                    Tester.Zamer($"List.Find(x => x == NotExistElement)", () => List.Find(x => x == NotExistElement), n);
                    Tester.Zamer($"List.FirstOrDefault()", () => List.FirstOrDefault(), n);
                    Tester.Zamer($"List.FirstOrDefault(x => x == ExistElement)", () => List.FirstOrDefault(x => x == ExistElement), n);
                    Tester.Zamer($"List.FirstOrDefault(x => x == NotExistElement)", () => List.FirstOrDefault(x => x == NotExistElement), n);
                    Tester.Zamer($"List.Where(x => x == ExistElement).FirstOrDefault()", () => List.Where(x => x == ExistElement).FirstOrDefault(), n);
                    Tester.Zamer($"List.LastOrDefault()", () => List.LastOrDefault(), n);
                    Tester.Zamer($"List.LastOrDefault(x => x == ExistElement)", () => List.LastOrDefault(x => x == ExistElement), n);
                    Tester.Zamer($"List.LastOrDefault(x => x == NotExistElement)", () => List.LastOrDefault(x => x == NotExistElement), n);
                    Tester.Zamer($"List.FindAll(x => x > 10)", () => List.FindAll(x => x > 10), n);
                    Tester.Zamer($"List.Where(x => x > 10).ToList()", () => List.Where(x => x > 10).ToList(), n);

                    Tester.Zamer($"List.Count(x => x == ExistElement)", () => List.Count(x => x == ExistElement), n);
                    Tester.Zamer($"List.Count(x => x == NotExistElement)", () => List.Count(x => x == NotExistElement), n);

                    Tester.Zamer($"List.Exists(x => x == ExistElement)", () => List.Exists(x => x == ExistElement), n);
                    Tester.Zamer($"List.Exists(x => x == NotExistElement)", () => List.Exists(x => x == NotExistElement), n);
                    Tester.Zamer($"List.Any(x => x == ExistElement)", () => List.Any(x => x == ExistElement), n);
                    Tester.Zamer($"List.Any(x => x == NotExistElement)", () => List.Any(x => x == NotExistElement), n);
                    Tester.Zamer($"List.Contains(ExistElement)", () => List.Contains(ExistElement), n);
                    Tester.Zamer($"List.Contains(NotExistElement)", () => List.Contains(NotExistElement), n);
                    TryGC();

                    ExistElement = Dictionary.Keys.ToArray()[localRandom.Next(Dictionary.Count)];
                    NotExistElement = Dictionary.Max(x => x.Key + x.Value) + 5;
                    Tester.Zamer($"Dictionary.Contains(ExistElement)", () => Dictionary.ContainsKey(ExistElement), n);
                    Tester.Zamer($"Dictionary.Contains(NotExistElement)", () => Dictionary.ContainsKey(NotExistElement), n);
                    Tester.Zamer($"Dictionary.ContainsValue(ExistElement)", () => Dictionary.ContainsValue(ExistElement), n);
                    Tester.Zamer($"Dictionary.ContainsValue(NotExistElement)", () => Dictionary.ContainsValue(NotExistElement), n);
                    TryGC();

                    ExistElement = Stack.ToArray()[localRandom.Next(Stack.Count)];
                    NotExistElement = Stack.Max() + 5;
                    Tester.Zamer($"Stack.FirstOrDefault()", () => Stack.FirstOrDefault(), n);
                    Tester.Zamer($"Stack.FirstOrDefault(x => x == ExistElement)", () => Stack.FirstOrDefault(x => x == ExistElement), n);
                    Tester.Zamer($"Stack.FirstOrDefault(x => x == NotExistElement)", () => Stack.FirstOrDefault(x => x == NotExistElement), n);
                    Tester.Zamer($"Stack.Contains(ExistElement)", () => Stack.Contains(ExistElement), n);
                    Tester.Zamer($"Stack.Contains(NotExistElement)", () => Stack.Contains(NotExistElement), n);
                    TryGC();

                    ExistElement = HashSet.ToArray()[localRandom.Next(HashSet.Count)];
                    NotExistElement = HashSet.Max() + 5;
                    Tester.Zamer($"HashSet.FirstOrDefault()", () => HashSet.FirstOrDefault(), n);
                    Tester.Zamer($"HashSet.FirstOrDefault(x => x == ExistElement)", () => HashSet.FirstOrDefault(x => x == ExistElement), n);
                    Tester.Zamer($"HashSet.FirstOrDefault(x => x == NotExistElement)", () => HashSet.FirstOrDefault(x => x == NotExistElement), n);
                    Tester.Zamer($"HashSet.Contains(ExistElement)", () => HashSet.Contains(ExistElement), n);
                    Tester.Zamer($"HashSet.Contains(NotExistElement)", () => HashSet.Contains(NotExistElement), n);
                    Tester.Zamer($"HashSet.Count(x => x == ExistElement)", () => HashSet.Count(x => x == ExistElement), n);
                    Tester.Zamer($"HashSet.Count(x => x == NotExistElement)", () => HashSet.Count(x => x == NotExistElement), n);
                    TryGC();

                    ExistElement = SortedList.Keys[localRandom.Next(SortedList.Count)];
                    NotExistElement = SortedList.Max(x => x.Key + x.Value) + 5;
                    Tester.Zamer($"SortedList.ContainsKey(ExistElement)", () => SortedList.ContainsKey(ExistElement), n);
                    Tester.Zamer($"SortedList.ContainsKey(NotExistElement)", () => SortedList.ContainsKey(NotExistElement), n);
                    Tester.Zamer($"SortedList.ContainsValue(ExistElement)", () => SortedList.ContainsValue(ExistElement), n);
                    Tester.Zamer($"SortedList.ContainsValue(NotExistElement)", () => SortedList.ContainsValue(NotExistElement), n);
                    Tester.Zamer($"SortedList.Count(x => x.Key == ExistElement)", () => SortedList.Count(x => x.Key == ExistElement), n);
                    Tester.Zamer($"SortedList.Count(x => x.Key == NotExistElement)", () => SortedList.Count(x => x.Key == NotExistElement), n);
                    TryGC();

                    ExistElement = Array[localRandom.Next(Array.Length)];
                    NotExistElement = Array.Max() + 5;
                    Tester.Zamer($"Array.FirstOrDefault()", () => Array.FirstOrDefault(), n);
                    Tester.Zamer($"Array.FirstOrDefault(x => x == ExistElement)", () => Array.FirstOrDefault(x => x == ExistElement), n);
                    Tester.Zamer($"Array.FirstOrDefault(x => x == NotExistElement)", () => Array.FirstOrDefault(x => x == NotExistElement), n);
                    Tester.Zamer($"Array.Contains(ExistElement)", () => Array.Contains(ExistElement), n);
                    Tester.Zamer($"Array.Contains(NotExistElement)", () => Array.Contains(NotExistElement), n);
                    Tester.Zamer($"Array.Count(x => x == ExistElement)", () => Array.Count(x => x == ExistElement), n);
                    Tester.Zamer($"Array.Count(x => x == NotExistElement)", () => Array.Count(x => x == NotExistElement), n);

                    ExistElement = ObservableCollection[localRandom.Next(ObservableCollection.Count)];
                    NotExistElement = ObservableCollection.Max() + 5;
                    Tester.Zamer($"ObservableCollection.FirstOrDefault()", () => ObservableCollection.FirstOrDefault(), n);
                    Tester.Zamer($"ObservableCollection.FirstOrDefault(x => x == ExistElement)", () => ObservableCollection.FirstOrDefault(x => x == ExistElement), n);
                    Tester.Zamer($"ObservableCollection.FirstOrDefault(x => x == NotExistElement)", () => ObservableCollection.FirstOrDefault(x => x == NotExistElement), n);
                    Tester.Zamer($"ObservableCollection.Contains(ExistElement)", () => ObservableCollection.Contains(ExistElement), n);
                    Tester.Zamer($"ObservableCollection.Contains(NotExistElement)", () => ObservableCollection.Contains(NotExistElement), n);
                    //Tester.Zamer($"ObservableCollection.Count(x => x == ExistElement)", () => ObservableCollection.Count(x => x == ExistElement));
                    //Tester.Zamer($"ObservableCollection.Count(x => x == NotExistElement)", () => ObservableCollection.Count(x => x == NotExistElement));
                    TryGC();

                    #endregion

                    #region Функции LINQ // todo всякие разные функции

                    Tester.Zamer($"List.ToList().Sort()", () => List.Sort(), n);
                    Tester.Zamer($"List.ToList().Sort((x, y) => (int)(x - y))", () => List.Sort((x, y) => (int)(x - y)), n);
                    Tester.Zamer($"Array.OrderBy(x => x).ToList()", () => Array.OrderBy(x => x).ToList(), n);
                    Tester.Zamer($"List.OrderBy(x => x).ToList()", () => List.OrderBy(x => x).ToList(), n);
                    Tester.Zamer($"BlockingCollection.OrderBy(x => x)", () => BlockingCollection.OrderBy(x => x).ToList(), n);
                    Tester.Zamer($"ObservableCollection.OrderBy(x => x)", () => ObservableCollection.OrderBy(x => x).ToList(), n);
         
                    Tester.Zamer($"List.ToList()", () => List.ToList(), n);
                    Tester.Zamer($"List.ToArray()", () => List.ToArray(), n);
                    Tester.Zamer($"List.GroupBy(x => x).ToList()", () => List.GroupBy(x => x).ToList(), n);
                    Tester.Zamer($"List.GroupBy(x => x).ToDictionary()", () => List.GroupBy(x => x).ToDictionary(x => x, x => x), n);
                    TryGC();

                    #endregion

                    #region Add
                    int k = 0;
                    Tester.Zamer($"List.Add(++i)", () => List.Add(++k), n);
                    Tester.Zamer($"HashSet.Add(++i)", () => HashSet.Add(++k), n);
                    Tester.Zamer($"Dictionary.Add(++i, i)", () => Dictionary.Add(++k, k), n);
                    Tester.Zamer($"SortedList.Add(++i, i)", () => SortedList.Add(++k, k), n);
                    Tester.Zamer($"ObservableCollection.Add(++i)", () => ObservableCollection.Add(++k), n);
                    Tester.Zamer($"BlockingCollection.Add(++i)", () => BlockingCollection.Add(++k), n);
                    TryGC();

                    #endregion
                    Tester.Stop();
                }

                //GC.EndNoGCRegion();
                if (i == 0) Tester.Results.Clear();
                StaticField += localField;
            }
            //Tester.Stop();
        }

        public string TestAndSave(string platform)
        {
            TestAndSave(platform, false);
            return TestAndSave(platform, true);
        }

        public string TestAndSave(string platform, bool forceGC)
        {
            _isGC = forceGC;
            Test();
            var data = new TimeTestResult(Tester.Results)
            {
                Platform = platform,
                IsDebuggerAttached = false, // todo
                IsGC = !forceGC,
            };
            try
            {
                BaseTimeTest.I.Create(data);
                return data.ToStringAll();
            }
            catch (Exception e)
            {
                return data.ToStringAll() + "\n\n" + e;
            }
        }
    }
}