﻿using System;

namespace TestLibrary
{
    /// <summary>
    /// Результат замера быстродействия
    /// (c) Konstatos
    /// </summary>
    public class TimeResult
    {
        public int GC;
        public int Count;
        public int Elements;
        public TimeSpan Time;

        public double Nanoseconds()
        {
            return Time.TotalMilliseconds / Count * 1000000;
        }

        public override string ToString()
        {
            return "\t" + Nanoseconds() + "\t" + Count;
        }
    }
}