﻿using System;
using System.Collections.Generic;
using System.Runtime;

namespace TestLibrary
{
    /// <summary>
    /// Механизм синхронного замера быстродействия
    /// (c) Konstatos
    /// </summary>
    public class TimeTest
    {
        private string GcCount = "";
        public readonly Dictionary<string, List<TimeResult>> Results = 
            new Dictionary<string, List<TimeResult>>();

        public void Zamer(string info, Action action, int count = 10000)
        {
            if (!Results.ContainsKey(info))
                Results.Add(info, new List<TimeResult>());
            var z = Zamer(action);
            if (z.GC == 0) Results[info].Add(z);
            else GcCount += $"{info}: {z.GC}\n";
        }

        private DateTime _start;

        private TimeResult Zamer(Action action, int count = 10000)
        {
            _start = DateTime.UtcNow;
            for (int i = 0; i < count; i++)
                action.Invoke();
            return new TimeResult()
            {
                Count = count,
                Time = DateTime.UtcNow.Subtract(_start),
            };
        }
    }
}
