﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TestLibrary
{
    public class BaseTimeTest : BaseRepository<TimeTestResult>
    {
        public static BaseTimeTest I = new BaseTimeTest();

        public List<List<object>> GetAllData()
        {
            var all = GetAll().ToList();
            if (all.Count == 0) return new List<List<object>>();

            //var first = all[0];
            //var s = first.ToStringTitle() + "\t" + TimeTestResult.ToStringTitleAll() + "\n";
            return all.Select(x =>
            {
                var begin = x.ToListData();
                //var data = x.ToStringDataAll(begin + "\t");
                return begin;
            }).ToList();
        }

        public string GetGroupDataString()
        {
            var d = new DateTime(2022, 2, 1);
            var all = GetAll().Where(x => x.Date > d).ToList();
            if (all.Count == 0) return "Нет данных";
            var grup = all.SelectMany(x => x.Results.Select(y => new
            {
                Platform = $"{x.Platform} ({(x.IsDebug ? "Debug" : "Release")} {(x.IsDebuggerAttached ? "+Debugger" : "")} {(x.IsGC ? "GC" : "")}, {x.PcInfo.ToStringSum()})",
                Func = y,
            })).SelectMany(x => x.Func.Value.Select(y => new
            {
                F = x.Func.Key + "\t" + y.Elements,
                Platform = x.Platform,
                Res = y,
            })).GroupBy(x => x.F).ToList();

            var sb = new StringBuilder();
            var platforms = grup.SelectMany(x => x.Select(y => y.Platform))
                .Distinct().ToList();
            sb.AppendLine("Функция\tЭлеметны\t" + string.Join("\t", platforms));
            foreach (var byFunc in grup)
            {
                var dic = byFunc.GroupBy(x => x.Platform)
                    .ToDictionary(x => x.Key, x => x.Select(y => y.Res).ToList());
                sb.Append(" " + byFunc.Key);
                foreach (var p in platforms)
                {
                    var median = "";
                    var zamers = 0;
                    if (dic.TryGetValue(p, out var times))
                    {
                        times = times.Where(x => x.Count > 0).ToList();
                        zamers = times.Count;
                        if (times.Count > 0)
                        {
                            var nano = times.Select(x => x.Nanoseconds()).OrderBy(x => x).ToList();
                            median = nano[nano.Count / 2].ToString();
                        }
                    }
                    sb.Append("\t" + median); // + "\t" + zamers);
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        public string GetAllDataString()
        {
            var d = new DateTime(2022, 2, 1);
            var all = GetAll().Where(x => x.Date > d).ToList();
            if (all.Count == 0) return "Нет данных";

            var first = all[0];
            var s = first.ToStringTitle() + "\t" + TimeTestResult.ToStringTitleAll() + "\n";
            s += string.Join("\n", all.Select(x =>
            {
                var begin = x.ToStringData();
                var data = x.ToStringDataAll(begin + "\t");
                return data;
            }));
            return s;
        }
    }

    public class BaseRepository<T> where T : IEntity
    {
        protected IMongoCollection<T> _collection;
        public readonly string Name;

        protected BaseRepository(bool global) { }

        public BaseRepository(string postfix = "s")
        {
            var databaseConnectionString = "mongodb://123:123@cluster0-shard-00-00.4wird.mongodb.net:27017,cluster0-shard-00-01.4wird.mongodb.net:27017,cluster0-shard-00-02.4wird.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-m0jbd0-shard-0&authSource=admin&retryWrites=true&w=majority";
            //var databaseConnectionString = "mongodb+srv://123:123@cluster0.4wird.mongodb.net/test";
            //var databaseConnectionString = "mongodb://localhost:27017";
            var client = new MongoClient(databaseConnectionString);
            var database = client.GetDatabase("Times");
            Name = typeof(T).Name.ToLower() + postfix;
            _collection = database.GetCollection<T>(Name);
        }

        public T GetAdd(T entity)
        {
            var r = _collection.FindSync(t => t._id == entity._id).FirstOrDefault();
            if (r != null) return r;
            Create(entity);
            return entity;
        }

        /// <summary>
        /// Полностью заменяет коллекцию, новую не создаёт
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(T entity)
        {
            _collection.ReplaceOne(t => t._id == entity._id, entity);
        }

        public void UpdateOrCreate(T entity)
        {
            _collection.ReplaceOne(t => t._id == entity._id, entity, new ReplaceOptions() { IsUpsert = true });
            //var r = _collection.FindSync(t => t._id == entity._id).FirstOrDefault();
            //if (r != null) Update(entity);
            //else Create(entity);
        }

        public void InsertMany(IEnumerable<T> all)
        {
            if (all.Any())
                _collection.InsertMany(all);
        }

        public void ReplaceAll(IEnumerable<T> all)
        {
            foreach (var entity in all)
                _collection.ReplaceOne(t => t._id == entity._id, entity, new ReplaceOptions() { IsUpsert = true });
            //_collection.InsertMany(all); // todo посмотреть еще пару раз на групповую замену
        }

        public virtual void Create(T entity)
        {
            _collection.InsertOne(entity);
        }
        public async Task AddManyAsync(IEnumerable<T> models)
        {
            await _collection.InsertManyAsync(models);
        }
        public void AddMany(IEnumerable<T> models)
        {
            _collection.InsertMany(models);
        }

        /// <summary> Меняет указанное поле первой найденной коллекции </summary> 
        public T UpdateFirst<TField>(Expression<Func<T, bool>> filter,
            Expression<Func<T, TField>> updateFieldSelect, TField newValue)
        {
            var update = Builders<T>.Update.Set(updateFieldSelect, newValue);
            return _collection.FindOneAndUpdate(filter, update);
        }

        /// <summary> Меняет указанное поле у нескольких коллекций, возвращает к-во измененных </summary> 
        public long UpdateMany<TField>(Expression<Func<T, bool>> filter,
            Expression<Func<T, TField>> updateFieldSelect, TField newValue)
        {
            var update = Builders<T>.Update.Set(updateFieldSelect, newValue);
            var res = _collection.UpdateMany(filter, update);
            return res.ModifiedCount;
        }

        /// <summary> Меняет указанное поле первой найденной коллекции:
        /// коллекция, выбор поля, значение поля </summary> 
        public T Update<TField>(T entity, Expression<Func<T, TField>> updateFieldSelect, TField newValue)
        {
            var update = Builders<T>.Update.Set(updateFieldSelect, newValue);
            return _collection.FindOneAndUpdate(t => t._id == entity._id, update);
        }

        public virtual void RemoveAll()
        {
            _collection.DeleteManyAsync(t => t._id != "");
        }

        public virtual void DeleteMany(Expression<Func<T, bool>> predicate)
        {
            _collection.DeleteManyAsync(predicate);
        }
        public virtual void Delete(string id)
        {
            _collection.DeleteOne(t => t._id == id);
        }

        public virtual List<T> SearchFor(Expression<Func<T, bool>> predicate)
        {
            //var r = _collection.AsQueryable().Where(predicate.Compile());
            var r = _collection.Find(predicate);
            return r.ToList();
        }
        public virtual IList<T> GetAllByOwner(string ownerId)
        {
            return _collection.FindSync(t => t.OwnerId == ownerId).ToList();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _collection.AsQueryable(); // new BsonDocument()).ToList();
        }

        public virtual T GetByOwnerId(string id)
        {
            return _collection.FindSync(t => t.OwnerId == id).FirstOrDefault();
        }
        public virtual T GetByName(string name)
        {
            return _collection.FindSync(t => t.Name == name).FirstOrDefault();
        }

        public virtual T GetById(string id)
        {
            return _collection.FindSync(t => t._id == id).FirstOrDefault();
        }

        public virtual long Count()
        {
            return _collection.EstimatedDocumentCount();
        }

        public virtual long Count(Expression<Func<T, bool>> filter)
        {
            return _collection.CountDocuments(filter);
        }

        #region K-MongoBase

        protected IMongoQueryable<T> Query => _collection.AsQueryable();

        public virtual IQueryable<T> Where(Expression<Func<T, bool>> expression)
        {
            return Query.Where(expression);
        }

        public IList<T> In<TField>(string field, List<TField> elements)
        {
            var filter = Builders<T>.Filter.In(field, elements);
            return _collection.Find(filter).ToList();
        }

        public async Task InsertOneAsync(T model)
        {
            await _collection.InsertOneAsync(model);
        }

        public async Task InsertManyAsync(IEnumerable<T> model)
        {
            if (model.Any())
                await Task.Run(() => AddMany(model));
            //await _collection.InsertManyAsync(model);
        }

        public async Task<T> UnsetAsync(Expression<Func<T, bool>> filter,
            Expression<Func<T, object>> updateFieldSelect)
        {
            var update = Builders<T>.Update.Unset(updateFieldSelect);
            return await _collection.FindOneAndUpdateAsync(filter, update);
        }

        public async Task ReplaceOneAsync(Expression<Func<T, bool>> filter, T newValue)
        {
            var res = await _collection.ReplaceOneAsync(filter, newValue);
            if (res.IsAcknowledged && res.ModifiedCount == 1) return;
            await _collection.InsertOneAsync(newValue);
        }

        public async Task DeleteManyAsync(Expression<Func<T, bool>> filter)
        {
            await _collection.DeleteManyAsync(filter);
        }

        public IMongoCollection<T> GetCollection()
        {
            return _collection;
        }

        public override string ToString() => Name + ": " + Count();
        public ICollection LoadAllTest() => GetAll().ToList();

        #endregion
    }
}
