# TimeTest

Небольшая переносимая бибилотека измерения производительности выполнения .Net функций в синхронном и ансинхронном вариантах.

Основные функциональные возможности следующие: 
1)	поток, постоянно выполняющий, переданную в библиотеку для изучения быстродействия, функцию из её делегата (ссылки на функцию private Action TestAction), что позволяет частично обойти оптимизацию повторяющихся операций механизмами .Net Framework-а;
2)	функция замера, принимающая делегат – замеряет количество выполнений этого делегата потоком в течение 1 миллисекунды;
3)	функция подсчета – накапливает результаты одинаковых замеров в словаре для последующего устранения пиковых результатов, усреднения, вычисления медианного значения;
4)	механизм минимизации вызова сборщика мусора и замера частоты его во время тестов.

Класс тестировщика:

    public class TimeTestAsync
    {
        public int TimeMilliseconds = 1;

        private Action TestAction = () => { };
        private Thread Thread;
        public int Count = 0;

        public readonly Dictionary<string, List<TimeResult>> Results = 
               new Dictionary<string, List<TimeResult>>();

        private void Init()
        {
            if (Thread != null) return;
            Thread = new Thread(TestFunk) { IsBackground = true };
            Thread.Start();
        }

        ~TimeTestAsync() => Stop();

        public void Zamer(string info, Action action)
        {
            Init();
            if (!Results.ContainsKey(info))
                Results.Add(info, new List<TimeResult>());
            var z = Zamer(action);
            Results[info].Add(z);
        }

        private DateTime _start;

        private TimeResult Zamer(Action action)
        {
            var gc = GC.CollectionCount(0);
            TestAction = action;
            Count = 0;
            _start = DateTime.UtcNow;
            Thread.Sleep(TimeMilliseconds);
            var end = DateTime.UtcNow.Subtract(_start);
            return new TimeResult()
            {
                Count = Count,
                Time = end,
                GC = GC.CollectionCount(0) - gc
            };
        }

        private void TestFunk()
        {
            while (true)
            {
                TestAction.Invoke();
                ++Count;
            }
        }

        public void Stop()
        {
            Thread?.Abort();
            Thread = null;
        }
    }

Результат замера выглядит следующим образом: 

    public class TimeResult
    {
        public int Count;
        public TimeSpan Time;
        public int GC;
        public double Nanoseconds() =>Time.TotalMilliseconds / Count * 1000000;
    }

По времени замера и количеству операций он определяет время выполнения тестируемой функции в наносекундах.

И собственно применение этого класса замеров возможно, например в таком виде:

    public class Tests
    {
        readonly TimeTestAsync Tester = new TimeTestAsync();
        private const string Br = "\t";
        private float ClassProperty { get; set; }
        static float StaticProperty { get; set; }
        static float StaticField = 0;
        float ClassField = 0;
        float ClassField2 = 0;
        string ClassStr = "";
        bool ClassBool = false;
        private const int Min = 50, Max = 100;
***
        public string Test()
        {
            var localRandom = new Random();

            for (int i = 0; i <= 10; i++)
            {
                ClassStr = "";
                ClassStringBuilder = new StringBuilder();
                ClassField = localRandom.Next(Min, Max);
***
                GC.Collect();
                Tester.Zamer("() => { ClassField++; }", () => { ClassField++; });
                Tester.Zamer("() => ClassStr = \"S1\"", () => ClassStr = "S1");
                Tester.Zamer("() => ClassStr = \"S1\" + ++ClassField", 
                              () => ClassStr = "S1" + ++ClassField);
                GC.Collect();
***
                if (i == 0) Tester.Results.Clear();
                StaticField += localField;
            }

            Tester.Stop();
            var tempCount = ClassField + StaticField;

            string s = $"Функция{Br}" +
                       $"Среднее время на выполнение, нс{Br}" +
                       $"Медиананное время на выполнение, нс{Br}" +
                       $"Среднеквадратичное отклонение{Br}" +
                       $"Среднее к-во запусков за тест, раз{Br}" +
                       $"Среднее Время теста, мс{Br}" +
                       $"К-во тестов, раз{Br}" +
                       $"Среднее к-во вызовов сборки мусора на тест, раз\n";
            foreach (var r in Tester.Results)
            {
                var withResults = r.Value.Where(x => x.Count > 0).ToArray();
                var nano = withResults.Select(x => x.Nanoseconds()).ToList();
                s += $"{r.Key}{Br}" +
                     $"{nano.Average(x => x):F2}{Br}" +
                     $"{nano.OrderBy(x => x).ToArray()[nano.Count / 2]:F2}{Br}" +
                     $"{StandardDeviation(nano):F2}{Br}" +
                     $"{withResults.Average(x => (double)x.Count):F0}{Br}" +
                     $"{withResults.Average(x => x.Time.TotalMilliseconds):F2}{Br}" +
                     $"{withResults.Length}{Br}" +
                     $"{withResults.Average(x => (double)x.GC):F2}\n";
            }
            s += "\n\n" + tempCount;// + "\n\n" + Tester.GcCount
            return s;
        }

